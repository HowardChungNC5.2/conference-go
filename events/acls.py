import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# code to make HTTP requests to Pexels and Open Weather Map
# should return Python dict to be used by view code
# to use keys from events/keys.py
# from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city} {state}"}

    response = requests.get(url, headers=headers, params=params)
    parsed_json = json.loads(response.content)
    photo_url = parsed_json["photos"][0]["src"]["original"]

    return photo_url


# get weather from third party app
def get_weather(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},001&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    # print("long/lat ", json.loads(response))
    parsed_json = json.loads(response.content)
    lat = parsed_json[0]["lat"]
    lon = parsed_json[0]["lon"]
    # print(f"lat & lon: {lat}{lon}")
    # https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather = requests.get(weather_url)
    parsed_weather = json.loads(weather.content)
    # print(f"parsed weather {parsed_weather}")
    result = parsed_weather["weather"][0]["description"]
    return result
